import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  APIURL = 'http://localhost:5000/api/users';
  apiurl = 'http://thedevshashank.com:5000/api/users';
  constructor(private http: HttpClient) { }

  // getDetails(id) {
  //   return this.http.get(`${this.url}?i=${id}&plot=full&apikey=${this.apiKey}`);
  // }

  getDetails() {
    console.log(this.http.get(this.APIURL));
    return this.http.get(this.APIURL);
  }
  getUsers(): Observable<any> {
    return this.http.get<any>(this.apiurl);
  }
}

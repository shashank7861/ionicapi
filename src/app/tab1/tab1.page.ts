import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  public users;
  constructor(
    private ds: DataService
  ) {}
  ngOnInit() {
    this.getData();
  }
  public getData() {
    console.log('Getting');
    this.ds.getUsers()
      .subscribe({                                                  // code that worked for sending observable to MatTableDataSource
        next: response => {
          const userPosts = response;
          // this.postData = userPosts.filter(
          //   post => post.state === 'published');
          this.users = userPosts;
          // console.log(this.postData);
          console.log(userPosts);
          return this.users;
        }
      });
  }

}
